To use this your `.aws/credentials` should look like this:

```ini
[default]
credential_process = pass aws «your aws token pass path here»
```

You need to move `aws.bash` into the directory documented in `pass(1)` and
enable extensions with `PASSWORD_STORE_ENABLE_EXTENSIONS=true`

The token pass record format is expected to be:
- first line: token
- second line: access key id
